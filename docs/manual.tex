\documentclass[twocolumn]{article}
\usepackage[british]{babel}
\usepackage{textcase}% provides \MakeTextUppercase (does not impact math mode)
\usepackage{amssymb, amsmath, amsthm, mathrsfs}
\usepackage{fancyhdr}
\usepackage{listings}
\usepackage{bussproofs} % proof tree
\usepackage[dvipsnames]{xcolor}
\usepackage{graphicx}
\RequirePackage[a4paper, left=2cm, right=2cm, bottom=3cm, top=2.3cm, headsep=100pt]{geometry}
\RequirePackage[small]{titlesec} % Taille des sections réduite
\RequirePackage[pdfborderstyle={/S/U/W 0}]{hyperref} % Le paramètre retire les bordures autour des hyperliens

\author{
  Arthur \textsc{Adjedj}\\
  Vincent \textsc{Lafeychine} \and
  Augustin \textsc{Albert} \\
  Lucas \textsc{Tabary-Maujean}
}

\title{
  \includegraphics[height=2.5cm]{media/logo}

	\textbf{Proost 0.1.0} \\
	User manual
  \\[1\baselineskip]\normalsize ENS Paris-Saclay
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Listings %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{paleblue}{rgb}{0.7,0.7,1}
\definecolor{darkgray}{rgb}{0.1,0.1,0.1}
\definecolor{dark}{rgb}{0,0,0}
\definecolor{forestgreen}{rgb}{0,0.3,0}
\definecolor{darkred}{rgb}{0.5,0,0}

\lstdefinestyle{default}{
    frame=tb,
    basicstyle=\ttfamily,
    numbers=left,
    numbersep=5pt,
    xleftmargin=1.5em,
    framexleftmargin=1em,
    numberstyle=\footnotesize\ttfamily\color{paleblue},
    keywordstyle={\bfseries\itshape\color{dark}},
    commentstyle=\color{gray},
    texcl, % Back to TeX styling within *inline* comments
    escapechar=`, % escape character for block comments
    %escapebegin=\lst@commentstyle,
    breaklines=true,
    breakatwhitespace=true
}
\lstdefinestyle{proost}{
    alsoletter={//,.,:,=},
    style=default,
    numberstyle=\scriptsize\ttfamily\color{paleblue},
    comment=[l]{//},
    keywords={fun, if},
    keywordstyle=\color{forestgreen},
		keywords=[2]{Prop, Type},
		keywordstyle=[2]\color{blue},
    keywords=[3]{def, check, eval, search, import},
    keywordstyle=[3]\color{darkred},
}
\lstnewenvironment{proost}[1][]{\lstset{style=proost, #1}}{}

\lstset{style=proost}
\lstMakeShortInline[columns=flexible]¤
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% headings %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\pagestyle{fancy}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}

\fancyhead{}
\fancyfoot[L]{\small{\reflectbox{\copyright} \the\year{}}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% bussproofs settings %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% documentation at https://mirror.ibcp.fr/pub/CTAN/macros/latex/contrib/bussproofs/BussGuide2.pdf
\EnableBpAbbreviations
% Overriding default label style
\def\RL#1{\RightLabel{\footnotesize\bfseries{#1}}}
\def\LL#1{\LeftLabel{\footnotesize\bfseries{#1}}}

% boxed proofs, used to align multiple proofs on a single line
\newenvironment{bprooftree}
  {\leavevmode\hbox\bgroup}
  {\DisplayProof\egroup}

\newcommand{\textr}[1]{{\footnotesize\textbf{\MakeTextUppercase{#1}}}}
% Fixes alignment issues but not without some consistency issues
\def\RL#1{\RightLabel{\makebox[0pt][l]{\textr{#1}}}}
\def\LL#1{\LeftLabel{\makebox[0pt][r]{\textr{#1}}}}
% consistent label (with width)
\def\cRL#1{\RightLabel{\textr{#1}}}
\def\cLL#1{\LeftLabel{\textr{#1}}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Divers %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\setlength{\columnsep}{20pt} % 10 pt par défaut

\newcommand{\members}[1]{\texorpdfstring{\hfill\scriptsize #1}{}}

\newcommand{\etun}{{\color{Green} ($\star$)} }
\newcommand{\etde}{{\color{Orange} ($\star\star$)} }
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\begin{document}
\maketitle
\thispagestyle{fancy}

\section{Introduction}
This program provides the user a set of tools to work with \(\lambda\)-terms as
described in the Calculus of Construction. Through the Curry-Howard
correspondence; users may associate a property or theorem they are willing to
prove to a type which can be expressed in CoC. Proving it then corresponds to
constructing a term of that type.

Release 0.1.0 of the project includes a toplevel interface also called
\texttt{proost}, which is the main way users can interact with this piece of
software. For a use directly through the crate APIs, please refer to their
respective documentations.

\section{Toplevel session}
In the toplevel, users are greeted with a prompt. There, they may enter the
following commands:
\begin{itemize}
	\item ¤import file1 file2¤ typechecks
		and loads the file in the current environment;
	\item ¤search v¤ looks for the definition of variable ¤v¤;
	\item ¤def a := t¤
		defines an alias ¤a¤ that can be used in any following command;
	\item ¤def a: ty := t¤ defines an alias ¤a¤ that is checked to
		be of type ¤ty¤;
	\item ¤check u: t¤ verifies ¤u¤ has type ¤t¤;
  \item ¤check u¤ provides the type of ¤u¤;
  \item ¤eval u¤ provides the definition of ¤u¤.
\end{itemize}

If the command succeeds, the toplevel returns a green check mark, with an
associated result if there is any. Otherwise, a red cross indicates an error
occurred, next to some details about it. The command is discarded and the user
may enter another command.

The toplevel provides to a certain extent history browsing, either \emph{via}
the up and down arrow keys or some autocompletion from previous commands.

\section{Language}
This release includes no standard library, which means users have to build their
theories from scratch. Besides, this version includes no notion of axioms, which
means terms have to be built from encodings within CoC.

Language syntax is as such:
\begin{itemize}
	\item Functions (\(\lambda\)-abstractions) are defined with the keyword ¤fun¤:
		¤fun x: A => u¤, ¤fun x y: A => u¤ (both ¤x¤ and ¤y¤ are of type ¤A¤),
		¤fun x: A, y: B => u¤ (multiple arguments, where ¤B¤ may depend on ¤x¤);

	\item Dependent function types (\(\Pi\)-types) are defined with a pair of parentheses
		before an arrow, as in: ¤(x: A) -> B¤, ¤(x y: A) -> B¤ or
		¤(x: A, y: B) -> C¤ (where the distinctions are similar to the previous
		item. Additionally, there is some usual syntactic sugar when the output
		type does not mention the input argument, which corresponds to usual
		function types: ¤A -> B¤, ¤A -> B -> C¤ (right-associativity);

	\item Function application of two terms ¤u¤ and ¤v¤ is simply written ¤u v¤,
		and is left-associative when there are multiple arguments;

	\item Variables are regular strings, which may only correspond to bound
		variables or previously defined terms;

	\item The type of propositions and higher-order types are written ¤Prop¤ and
		¤Type i¤, as usual.
\end{itemize}

This release includes a single example file \texttt{contraposition.mdln} which
defines elementary propositional calculus operators and their constructors.
\end{document}
